<?php

/**
 * @file zeest_slideshow.admin.inc
 */

function zeest_slideshow_form($form, &$form_state, $zeest_slideshow) {

  $form['name'] = array(
    '#title'         => t('Name'),
    '#type'          => 'textfield',
    '#default_value' => isset($zeest_slideshow->name) ? $zeest_slideshow->name : '',
    '#description'   => t('Provide a human name for the slideshow.'),
    '#required'      => TRUE,
  );


  $form['machine_name'] = array(
    '#type'          => 'machine_name',
    '#title'         => t('Machine Name'),
    '#default_value' => isset($zeest_slideshow->machine_name) ? $zeest_slideshow->machine_name : '',
    '#maxlength'     => MENU_MAX_MENU_NAME_LENGTH_UI,
    '#machine_name'  => array(
      'exists'          => 'zeest_slideshow_name_exists',
      'source'          => array('name'),
      'label'           => t('Machine Name'),
      'replace_pattern' => '[^a-z0-9_]+',
      'replace'         => '_',
    ),
    // Only enable while the create section entity
    //'#disabled' => !empty($zeest_slideshow->machine_name),
  );

  $form['enable_pagers'] = array(
    '#title'         => t('Activer le bloc Pagers'),
    '#type'          => 'checkbox',
    '#default_value' => isset($zeest_slideshow->enable_pagers) ? $zeest_slideshow->enable_pagers : TRUE,
    '#description'   => t('Cette case à cocher permet d\'avoir le bloc Pages'),
  );

  field_attach_form('zeest_slideshow', $zeest_slideshow, $form, $form_state);

  // Add the fields from the Field UI module.
  //field_attach_form('machine_creation', $machine_creation, $form, $form_state);
  $form['actions'] = array(
    '#type'  => 'actions',
    'submit' => array(
      '#type'  => 'submit',
      '#value' => isset($zeest_slideshow->id) ? t('Update slideshow') : t('Save the new slideshow'),
    ),
  );


  return $form;
}


/**
 * Submit handler for machine creation entity form.
 */
function zeest_slideshow_form_submit($form, &$form_state) {
  $machine = entity_ui_form_submit_build_entity($form, $form_state);
  $machine->save();
  drupal_set_message(t('@name has been saved.', array('@name' => $machine->name)));
  $form_state['redirect'] = 'admin/content/zeest/slideshow';
}
