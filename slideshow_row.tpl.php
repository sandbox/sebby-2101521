<?php


/**
 * @file slideshow_jhome.tpl.php
 *
 *
 * $variables:
 * - title: le titre du slide
 * - image : Image
 * - position :  Pavé de texte
 * - link: Object link avec les propriétés url, title.
 */

?>

<div class="slide-content">
    <div class="picture">
        <?php print $variables['image'];?>
    </div>
    <div class="contextual-content">
        <div class="title">
            <?php print $variables['title'];?>
        </div>
        <a class="link" href="<?php print $variables['link']['url'];?>" <?php ($variables['link']['attributes']['target'] == 0)? '' : 'target="_blank"' ?>>
            <?php print $variables['link']['title'];?>
        </a>
    </div>
</div>
