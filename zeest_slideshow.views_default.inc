<?php
/**
 * @file
 * zeest_slideshow.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function zeest_slideshow_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'slideshow';
  $view->description = 'Afficher un Slideshow en utilisant l\'entité et son field collection';
  $view->tag = 'slideshow';
  $view->base_table = 'field_collection_item';
  $view->human_name = 'Slideshow';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Slideshow Create';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['use_more_text'] = 'plus';
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Appliquer';
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Réinitialiser';
  $handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Trier par';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['type'] = 'ol';
  $handler->display->display_options['style_options']['class'] = 'slides';
  $handler->display->display_options['style_options']['wrapper_class'] = '';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relation: Field collection item : Entity with the Slide (field_slide) */
  $handler->display->display_options['relationships']['field_slide_zeest_slideshow']['id'] = 'field_slide_zeest_slideshow';
  $handler->display->display_options['relationships']['field_slide_zeest_slideshow']['table'] = 'field_collection_item';
  $handler->display->display_options['relationships']['field_slide_zeest_slideshow']['field'] = 'field_slide_zeest_slideshow';
  $handler->display->display_options['relationships']['field_slide_zeest_slideshow']['required'] = TRUE;
  /* Relation: Entity Reference : Referenced Entity */
  $handler->display->display_options['relationships']['field_page_interne_target_id']['id'] = 'field_page_interne_target_id';
  $handler->display->display_options['relationships']['field_page_interne_target_id']['table'] = 'field_data_field_page_interne';
  $handler->display->display_options['relationships']['field_page_interne_target_id']['field'] = 'field_page_interne_target_id';
  /* Champ: Contenu : Titre */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['relationship'] = 'field_page_interne_target_id';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Champ: Field collection item : Page interne */
  $handler->display->display_options['fields']['field_page_interne']['id'] = 'field_page_interne';
  $handler->display->display_options['fields']['field_page_interne']['table'] = 'field_data_field_page_interne';
  $handler->display->display_options['fields']['field_page_interne']['field'] = 'field_page_interne';
  $handler->display->display_options['fields']['field_page_interne']['label'] = '';
  $handler->display->display_options['fields']['field_page_interne']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_page_interne']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_page_interne']['type'] = 'entityreference_entity_id';
  $handler->display->display_options['fields']['field_page_interne']['settings'] = array(
    'view_mode' => 'full',
  );
  /* Champ: Champ : Visuel packaging */
  $handler->display->display_options['fields']['field_image']['id'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['table'] = 'field_data_field_image';
  $handler->display->display_options['fields']['field_image']['field'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['label'] = '';
  $handler->display->display_options['fields']['field_image']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_image']['alter']['path'] = '[field_external_link]';
  $handler->display->display_options['fields']['field_image']['alter']['alt'] = '[title]';
  $handler->display->display_options['fields']['field_image']['element_type'] = '0';
  $handler->display->display_options['fields']['field_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_image']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image']['settings'] = array(
    'image_style' => 'slideshow',
    'image_link' => '',
  );
  /* Champ: Field collection item : External Link */
  $handler->display->display_options['fields']['field_external_link_1']['id'] = 'field_external_link_1';
  $handler->display->display_options['fields']['field_external_link_1']['table'] = 'field_data_field_external_link';
  $handler->display->display_options['fields']['field_external_link_1']['field'] = 'field_external_link';
  $handler->display->display_options['fields']['field_external_link_1']['label'] = '';
  $handler->display->display_options['fields']['field_external_link_1']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_external_link_1']['alter']['text'] = '[field_image] ';
  $handler->display->display_options['fields']['field_external_link_1']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_external_link_1']['alter']['path'] = '[field_external_link_1-url]';
  $handler->display->display_options['fields']['field_external_link_1']['alter']['alt'] = '[field_external_link_1-title] ';
  $handler->display->display_options['fields']['field_external_link_1']['element_type'] = '0';
  $handler->display->display_options['fields']['field_external_link_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_external_link_1']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_external_link_1']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_external_link_1']['click_sort_column'] = 'url';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block_1');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  $handler->display->display_options['block_description'] = 'Slideshow HP';
  $handler->display->display_options['block_caching'] = '-1';

  /* Display: Pager */
  $handler = $view->new_display('attachment', 'Pager', 'attachment_1');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['class'] = 'pager';
  $handler->display->display_options['style_options']['wrapper_class'] = '';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Champ: Global : Texte personnalisé */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<a href="#"></a>';
  $handler->display->display_options['fields']['nothing']['element_type'] = '0';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['nothing']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['nothing']['element_default_classes'] = FALSE;
  /* Champ: Field collection item : Identifiant de Field collection item */
  $handler->display->display_options['fields']['item_id']['id'] = 'item_id';
  $handler->display->display_options['fields']['item_id']['table'] = 'field_collection_item';
  $handler->display->display_options['fields']['item_id']['field'] = 'item_id';
  $handler->display->display_options['fields']['item_id']['exclude'] = TRUE;
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  $handler->display->display_options['displays'] = array(
    'block_1' => 'block_1',
    'default' => 0,
    'panel_pane_1' => 0,
  );
  $handler->display->display_options['attachment_position'] = 'after';

  /* Display: Block 2 */
  $handler = $view->new_display('block', 'Block 2', 'block_2');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Slideshow LPM';
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['arguments'] = FALSE;

  /* Display: Pager */
  $handler = $view->new_display('attachment', 'Pager', 'attachment_3');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['class'] = 'pager';
  $handler->display->display_options['style_options']['wrapper_class'] = '';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Champ: Global : Texte personnalisé */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<a href="#"></a>';
  $handler->display->display_options['fields']['nothing']['element_type'] = '0';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['nothing']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['nothing']['element_default_classes'] = FALSE;
  /* Champ: Field collection item : Identifiant de Field collection item */
  $handler->display->display_options['fields']['item_id']['id'] = 'item_id';
  $handler->display->display_options['fields']['item_id']['table'] = 'field_collection_item';
  $handler->display->display_options['fields']['item_id']['field'] = 'item_id';
  $handler->display->display_options['fields']['item_id']['exclude'] = TRUE;
  $handler->display->display_options['displays'] = array(
    'block_2' => 'block_2',
    'default' => 0,
    'block_1' => 0,
  );
  $handler->display->display_options['attachment_position'] = 'after';
  $translatables['slideshow'] = array(
    t('Master'),
    t('Slideshow Create'),
    t('plus'),
    t('Appliquer'),
    t('Réinitialiser'),
    t('Trier par'),
    t('Asc'),
    t('Desc'),
    t('field_slide'),
    t('Contenu entity referenced from field_page_interne'),
    t('[title]'),
    t('[field_image] '),
    t('[field_external_link_1-title] '),
    t('Block'),
    t('Slideshow HP'),
    t('Pager'),
    t('<a href="#"></a>'),
    t('Identifiant de Field collection item'),
    t('.'),
    t(','),
    t('Block 2'),
    t('Slideshow LPM'),
  );
  $export['slideshow'] = $view;

  return $export;
}
