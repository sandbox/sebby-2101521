<?php
//var_dump($variables);

/**
 * @file slideshow_jhome.tpl.php
 *
 *
 * $variables:
 * - title: le titre du slideshow
 * - id : identifiant du slideshow
 * - controls : Afficher les controleurs
 * - pagers :  Afficher la pagination si l'option est activé
 * - slides : Afficher le contenu des slides
 */
?>

<div class="slideshow horizontal">
  <?php print $variables['controls'];?>

  <?php print $variables['slides'];?>

  <?php if(isset($variables['pagers'])):?>
  <?php print $variables['pagers'];?>
  <?php endif;?>
</div>
